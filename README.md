# Nextools Instructions

Welcome to the Nextools Challenge! 🛍️🚀

## Challenge Overview

In this challenge, you will be building a Shopify app using React. The goal is to assess your ability to integrate with the Shopify platform, implement session storage, and create a user interface using Shopify Polaris components.

## Task Description

1. **Create a Shopify Partner Account:**
   - Go to [Shopify Partners](https://partners.shopify.com/) and create a new account if you don't have one.

2. **Create a Demo Store:**
   - Within the Shopify Partner Dashboard, create a demo store for testing your app.

3. **Create a New React App Using Shopify CLI:**
   - Use the Shopify CLI to create a new React app.
   - Choose either the node boilerplate or the remix boilerplate.

4. **Implement Session Storage:**
   - Implement session storage to handle the installation process.
   - Choose either SQLite, Redis, or a custom session storage of your choice.

5. **Install the App in the Demo Store:**
   - Integrate your app with the demo store you created.

6. **Create UI Using Shopify Polaris Components:**
   - Design a user interface similar to the provided screenshot using Shopify Polaris React components.

7. **Implement Product Creation Functionality:**
   - Create a form to add a new product with the following attributes:
     - Title
     - Description
     - Image
     - Tag
     - At least one custom metafield with a key, and value.

8. **Save Product in Shopify Store:**
   - Ensure that the new product is correctly saved and visible in the Shopify store's products section.

## Bonus Tasks

1. **Implement Toast Notifications:**
   - Provide toast notifications to notify users of the successful saving/creation of the product.

2. **Use GraphQL:**
   - Implement the query to create the product using GraphQL instead of Rest API.

3. **Implement an app dashboard:**
   - Implement a dashboard inside the app that show a table with all the custom products created (tip: use tags to identify custom products ).

4. **Deploy as a Docker Container:**
   - Bonus points for deploying the app as a Docker container.

## Submission

Submit your solution as Gitlab (no github) repository containing the entire project. Include any additional instructions or comments you'd like to share about your implementation.

## Useful link

- [Create Shopify App](https://shopify.dev/docs/apps/getting-started/create)
- [Create Development Store](https://help.shopify.com/en/partners/dashboard/managing-stores/development-stores)
- [Shopify Admin GraphQL](https://shopify.dev/docs/api/admin/getting-started)
- [Shopify GraphQL Reference](https://shopify.dev/docs/api/admin-graphql)

## Evaluation Criteria

Your solution will be evaluated based on:

- Successful integration with the Shopify platform.
- Proper implementation of session storage.
- User interface design using Shopify Polaris components.
- Correct handling of product creation with required attributes.
- Bonus points for bonus tasks.

Good luck! 👩‍💻👨‍💻
